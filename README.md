# CS529_Naive_Bayes_and_Logistic_Regression

This project implements Naive Bayes and Logistic Regression on a toy data set
filled with word counts from news articles that need categorization into 1 of 20
different catagories.

The two files used for running the code are naiveBayes.py and 
logisticRegression.py. Both have been tested with python 3.6.6. All examples
are executed under line of code at the bottom of the file specified by

if __name__ == '__main__':

********************************************************************************
Usage for naiveBayes.py
********************************************************************************
Examples of runs for naiveBayes.py are as follows:
    test_run = Naive_Bayes_Classifier('training_matrix_rows_12000.npz', \
                                          'result_matrix_rows_12000.npz', \
                                          'testing_matrix.npz')

This will run with all the default settings on the full 12000 row training 
matrix and the full 6774 row testing matrix. As of the writing of this readme
the defaults are as follows.

Note: if the matrices you are requesting to load do not exist, then 
naiveBayes.py will run model = doc_data.data_model() which will attempt to
create a full 12000 row training set with a full result matrix (chose to split
the results from the frequency matrix).

model_tune = True
This will send the matrix into the model_tune function. This function will
turn certain columns into all zeros for words that you would like to exclude.
However this does not as work as well as intended as removing words tended to
decrease performance. For this to truly work the one_scalar in the
build_likelihood_matrix function would have to be modified to be zero where
words had been zeroed out. Experimenting with this proved unsuccessful. Thus
we recommend against modifying the parameter.

lower_thresh = 0
upper_thresh = 200000
These two parameters allow allow the user to zero out columns in model tune for
all words with frequency less than lower_thresh and greater than upper_thresh.
This allows us to get reduce words like is and archive down to 0 frequency in
the text matrix. Their probability is still not zero though, but some very small
value. Experimentation with changing these parameters (200,000 is higher than
any one words overall occurance) showed a reduction in accuracy.

view_freq = False
This option opens up a set of print outs in the build_frequency_vector function
these are for the user to view frequncy counts, where they occur in a matrix,
what words they occur, and how many words have what frequency. This is set to
false to avoid this from running normally, but if the user would like to see
a print out of this information it may be useful to set the option as True.

beta_set = False
beta = 1
These two parameters allow us to set the beta value. This controls how small of
a probability we will allow to exist for words that have near zero frequency.

val_run = False
k_folds = 8
make_con_mat = False
make_word_con_vec = False
The val_run parameter determines whether the a validation run with k-folds 
crossvalidation is executed. The k_folds parameter tells the program how many
folds to use. the make_con_mat determines whether a confusion matrix is
displayed. The make_word_con_vec determines whether a word count for incorrectly
classified records is displayed. Note both the confusion matrix and word count
vector are still calculated on every run when a validation is selected.

An example of a typical run for the naiveBayes.py is as follows
    test_run = Naive_Bayes_Classifier('training_matrix_rows_12000.npz', \
                                          'result_matrix_rows_12000.npz', \
                                          'testing_matrix.npz', \
                                          val_run = False, \
                                          beta_set = True, \
                                          beta = 0.0105, \
                                          k_folds = 12, \
                                          upper_thresh = 200000, \
                                          make_con_mat = False, \
                                          view_freq = False)

Here we are doing a test run for submission to kaggle. The val_run is not set
and due to the high upper threshold no words are removed.

An example of running the classifier many time to get a range of accuracies for 
given beta values is as follows.
    average_list = []
    for item0 in [i/10000 for i in range(90, 120, 1)]:
        start_time = datetime.now()
        test_run = Naive_Bayes_Classifier('training_matrix_rows_12000.npz', \
                                          'result_matrix_rows_12000.npz', \
                                          'testing_matrix.npz', \
                                          val_run = True, \
                                          beta_set = True, \
                                          beta = item0, \
                                          k_folds = 24)
        print("Validation time taken was, ", datetime.now() - start_time)        
        average_list.append((item0,test_run.average_accuracy))
    print(average_list)

This makes a run through beta values from 0.009 to 0.0120 and measures the
average accuracy across the dataset. It prints out the validation time
taken and a list of average accuracies for each beta value.
********************************************************************************
Usage for logisticRegression.py
********************************************************************************
Examples of runs for logisticRegression.py are as follows:

	test_run = Logistic_Regression_Classifier('training_matrix_rows_12000.npz', \
                                              'result_matrix_rows_12000.npz', \
                                              'testing_matrix.npz')

This will run with all the default settings on the full 12000 row training 
matrix and the full 6774 row testing matrix. As of the writing of this readme
the defaults are as follows.

Note: if the matrices you are requesting to load do not exist, then 
logisticRegression.py will run model = doc_data.data_model() which will attempt 
to create a full 12000 row training set with a full result matrix (chose to split
the results from the frequency matrix).

stepsize = 0.01
The stepsize controls how big of steps we take in the gradent descent curve.
This contols how fast we get to the minimum of the curve, but also may cause
problems with convergence if our stepsize is too large when we get close to the
minimum value. This should be varied by the user to get the ideal convergence
rate and maximum accuracy.

regularization = 0.0001
The regularization controls the bias for the iterative scheme. The larger the
regularization the more the bias is controlled. This term is a user defined
parameter and needs to be adjusted given the problem.

iteration_number = 10000
The iteration_number gives the maximum iterations that can occur in a given
run. For the k-folds cross validation this is reduced to a lower value after
the first k-fold sample taking the iteration number when the tolerance threshold
is met. After this that iteration number is set for the other k-fold
occurances. In the nonvalidation case, the method simply runs for the
iteration_number amount of loops. This should be adjusted based on the problem.

acc_tolerance = 0.0001
The accuracy tolerance parameter is used to set an iteration limit in a
k-folds cross validation run of the method. At the first convergence, of a 
k-fold iteration, iteration_number is set and for all subsequent k-folds are
limited to that iteration limit. This parameter should be set by the user for a
given dataset.
 
model_tune = True
This will send the matrix into the model_tune function. This function will
turn certain columns into all zeros for words that you would like to exclude.
However this does not as work as well as intended as removing words tended to
decrease performance. For this to truly work the one_scalar in the
build_likelihood_matrix function would have to be modified to be zero where
words had been zeroed out. Experimenting with this proved unsuccessful. Thus
we recommend against modifying the parameter.

lower_thresh = 0
upper_thresh = 200000
These two parameters allow allow the user to zero out columns in model tune for
all words with frequency less than lower_thresh and greater than upper_thresh.
This allows us to get reduce words like is and archive down to 0 frequency in
the text matrix. Their probability is still not zero though, but some very small
value. Experimentation with changing these parameters (200,000 is higher than
any one words overall occurance) showed a reduction in accuracy.

view_freq = False
This option opens up a set of print outs in the build_frequency_vector function
these are for the user to view frequncy counts, where they occur in a matrix,
what words they occur, and how many words have what frequency. This is set to
false to avoid this from running normally, but if the user would like to see
a print out of this information it may be useful to set the option as True.

beta_set = False
beta = 1
These two parameters allow us to set the beta value. This controls how small of
a probability we will allow to exist for words that have near zero frequency.

val_run = False
k_folds = 8
make_con_mat = False
make_word_con_vec = False
The val_run parameter determines whether the a validation run with k-folds 
crossvalidation is executed. The k_folds parameter tells the program how many
folds to use. the make_con_mat determines whether a confusion matrix is
displayed. The make_word_con_vec determines whether a word count for incorrectly
classified records is displayed. Note both the confusion matrix and word count
vector are still calculated on every run when a validation is selected.

An example of a typical run for the logisticRegression.py is as follows
test_run = Logistic_Regression_Classifier('training_matrix_rows_12000.npz', \
                                          'result_matrix_rows_12000.npz', \
                                          'testing_matrix.npz', \
                                          val_run = False, \
                                          k_folds = 3, \
                                          iteration_number = 5000, \
                                          stepsize = 0.005, \
                                          regularization = 0.0001)

Here we are doing a test run for submission to kaggle. The val_run is not set.

For a validation run:
test_run = Logistic_Regression_Classifier('training_matrix_rows_12000.npz', \
                                          'result_matrix_rows_12000.npz', \
                                          'testing_matrix.npz', \
                                          val_run = True, \
                                          k_folds = 3, \
                                          iteration_number = 15000, \
                                          stepsize = 0.005, \
                                          regularization = 0.0001)

For a loop of validation runs through a set of stepsizes we can use the 
following:

average_list = []
for item0 in [i/10000 for i in range(10, 110, 10)]:
#start_time = datetime.now()
test_run = Logistic_Regression_Classifier('training_matrix_rows_12000.npz', \
                                      'result_matrix_rows_12000.npz', \
                                      'testing_matrix.npz', \
                                      val_run = True, \
                                      k_folds = 3, \
                                      iteration_number = 20000, \
                                      stepsize = item0, \
                                      regularization = 0.009)
#print("Validation time taken was, ", datetime.now() - start_time)        
average_list.append((item0,test_run.regularization,test_run.average_accuracy))
print(average_list)





