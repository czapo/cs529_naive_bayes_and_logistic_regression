#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Authors: Christopher Zapotocky and Francisco Viramontes
Created: October 15, 2018
Modified: October 15, 2018
Program Name: logisticRegression.py
Program Function: The program performs the Logistic Regression
classification on a set of data. Particularly a set of
word frequencies taken from a number of documents.
"""
#Incase the matricies we request are not there
import doc_data
import numpy as np
#Import all the sparse matricies libraries
from scipy.sparse import vstack, hstack, diags, eye, find
from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
from scipy.sparse import load_npz
from scipy.sparse.linalg import inv, svds
from scipy.linalg import diagsvd
#We need os.path to go find the saved sparse matrices
import os
import os.path
#We need dattime to run the timer to see how we are doing
from datetime import datetime
#We need math because math is important...and we need to take logs
import math
#This is experimentally used. I was trying to remove words from the matrix
#by zeroing out there probability. Works like log normally, but sets
#log(0) = 0
from scipy.special import xlogy
#Remember lists are not deep copied and can be effected by replacement 
import copy
#We need this to shuffle the row values to make the validation sets
import random
import time

#Have a look at the readme to find out what each option does and how it is used
class Logistic_Regression_Classifier:
    def __init__(self,training_name = 'training_matrix.npz', \
                 result_name = 'result_matrix.npz', \
                 testing_name = 'testing_matrix.npz', \
                 stepsize = 0.01, \
                 regularization = 0.0001, \
                 iteration_number = 10000, \
                 acc_tolerance = 0.0001, \
                 model_tune = True, \
                 lower_thresh = 0, \
                 upper_thresh = 200000, \
                 view_freq = False, \
                 val_run = False, \
                 k_folds = 8, \
                 make_con_mat = False, \
                 make_word_con_vec = False):
        if not (os.path.exists(training_name)) \
            and not(os.path.exists(result_name)):
            print("We could not find any matrices so" \
                    " we are going to make some.")
            model = doc_data.data_model()

        #Load matrices for computation of Naive Bayes
        self.text_matrix = load_npz(training_name)
        self.result_matrix = load_npz(result_name)
        self.testing_matrix = load_npz(testing_name)

        #We would like the text matrix to be a set of floats an not ints.
        self.text_matrix.asfptype()

        #Here we get the number of classes in the result matrix this is
        #important to the program.
        self.classes = int(self.result_matrix.max())

        #Stored class variables during initialization
        self.stepsize = stepsize
        self.regularization = regularization
        self.iteration_number = iteration_number
        self.acc_tolerance = acc_tolerance
        self.tol_set = False
        self.upper_thresh = upper_thresh
        self.lower_thresh = lower_thresh
        self.k_folds = k_folds
        self.view_freq = view_freq
        self.removed_words = 0
        self.accuracy = []
        self.average_accuracy = None
        self.make_con_mat = make_con_mat
        self.make_word_con_vec = make_word_con_vec
        
        #We ask here if this is a validation run if so then validation subsets
        #are built and the self.text_matrices and self.result_matrices are
        #varied. Else...we run through the full data set.
        self.val_run = val_run

        #This is just to check how long the program takes to run.
        start_time = datetime.now()
        print("The function started at: ", start_time)
        
        #Only run the model tuning if we postively select it
        if model_tune == True:
            self.build_frequency_vector()

        #Here we run method internally in the initialization function

        #For a kaggle run we want to use the full data set with no validation
        #the if here does that
        if not val_run:
            self.primary_function_calls()
        #If validation is indeed set then we run the else statement here
        else:
            #This function calculate the k-folds that are necessary to build
            #the validation matrices on every step both text and result
            self.build_k_fold_validation()

            #Here we instantiate for the confusion matrix and
            #word_confusion_vector
            self.confusion_matrix = np.zeros((self.classes,self.classes))
            self.word_confusion_vector = np.zeros((1,self.testing_matrix.shape[1]))
            self.word_confusion_vector = csr_matrix(self.word_confusion_vector)

            #After building our validation partition we have to rebuild the text
            #matrix minus one partition.
            for part in range(0,self.k_folds):
                print("The current partition being tested is partition " + str(part) + ".")
                #For this round do the following:
                
                #Build the validation training set
                temp_train_list = copy.deepcopy(self.val_comp_list)
                del temp_train_list[part]

                #Build the validtion training result set
                temp_result_list = copy.deepcopy(self.res_comp_list)
                del temp_result_list[part]

                #Build the validation testing result set
                temp_test_result_list = copy.deepcopy(self.res_comp_list[part])

                #Build the validation testing set
                temp_test_mat = copy.deepcopy(self.val_comp_list[part])

                #We constantly rebuild the full training set by stacking the set
                #of sparse matrices
                self.text_matrix = vstack(temp_train_list)
                self.result_matrix = vstack(temp_result_list)

                #Since we only have one partition being tested...not stacking is
                #needed in this part
                self.testing_matrix = temp_test_mat
                self.test_comp_result_mat = temp_test_result_list

                #Run everything with the primary functions as previously.
                self.primary_function_calls()
                accuracy_output = self.validation_testing_accuracy()
                self.accuracy.append(accuracy_output)

            self.average_accuracy = sum(self.accuracy)/len(self.accuracy)             
            print("The average accuracy at " + str(self.stepsize) + " is " \
                    + str(self.average_accuracy) + ".")
            print("\n")
            #This prints the confusion matrix and word_confusion_vector if
            #we have those flags set
            if self.make_word_con_vec:
                np.set_printoptions(threshold=np.nan)
                print(self.word_confusion_vector)
            if self.make_con_mat:            
                np.set_printoptions(linewidth = 180)
                print(self.confusion_matrix)
        #We print out the time taken for the validation run here.
        print("Function time taken: ", datetime.now() - start_time)
                
    def primary_function_calls(self):
        #We need to reduce the size of the text matrix
        #Implement this later.
        self.text_matrix = self.normalize_text_matrix(self.text_matrix)
        
        #This just adds a w0 weight column to the end of the text matrix
        self.text_matrix = self.add_ones_col_to_matrix(self.text_matrix)

        #Lets initialize the weight matrix to zero  
        self.build_random_weight_matrix()

        i = 0
        current_acc = 0
        previous_acc = 0
        while i <= self.iteration_number:
            #Here is where we find the weights and iterate through GA
            self.find_prob_class_given_data()

            #Find new weight matrix
            max_dif = self.find_new_weight_matrix()
            if self.val_run == True:
                if i%200 == 0:
                    print("The sum of the squared errors for iteration " \
                          + str(i) + " is: ", max_dif)
                    self.classify()
                    #Here we check if the tolerance differance has been met
                    #between iterations.
                    current_acc = self.validation_testing_accuracy()
                    current_tol = abs(current_acc - previous_acc)
                    previous_acc = copy.deepcopy(current_acc)
                    print("The current accuracy is: ", previous_acc)
                    #If the tolerance threshold has been met then set the max
                    #iterations to occur for every k_fold check.
                    if (current_tol < self.acc_tolerance) and (self.tol_set == False):
                        self.iteration_number = i
                        self.tol_set = True
            else:
                if i%200 == 0:
                    print("The sum of the squared errors for iteration " \
                          + str(i) + " is: ", max_dif)
                    #Since this is an online method we can classify as we go and
                    # improve the method if more data is added.
                    self.classify(i)
            i = i + 1

    def build_k_fold_validation(self):
        #We assume the user wants a "leave one partition out" k_folds
        #validation here
        
        #This is going to be a list of pointers to the matricies
        #specified by the kfolds cross validiation
        self.val_comp_list = []
        self.res_comp_list = []

        #Make a list of the row numbers for the text matrix.
        row_num_list = [j for j in range(0,self.text_matrix.shape[0])]

        #Then we shuffle the row numbers to make a randomization of the
        #row numbers. This is done inplace...so no need to assign the output.
        random.shuffle(row_num_list)

        #Now that we have randomly shuffled the row numbers we are going to
        #take all the n mod(k) = 0 and put it in partition 0. We will do the
        #same for 1,2, up to k-1. That will give us our k, random, partitions.

        k_fold_rem = self.text_matrix.shape[0] % self.k_folds
        j = math.floor(self.text_matrix.shape[0]/self.k_folds)
        total_rows_done = 0

        for row_mod in range(0,self.k_folds):
            #We want to balance our valdiation as much as possible so we see
            #what the remainder is from the division by k-folds. Our first j
            #matrices we build will have 1 extra row over the
            #floor(rows_text_matrix/k_folds).

            #Which side of the excess from the remainder are we on right now?
            if row_mod < k_fold_rem:
                s = j + 1
            else:
                s = j

            #Build the validation matrix partition
            new_val_matrix = csr_matrix((s,self.text_matrix.shape[1]))
            new_res_matrix = csr_matrix((s,1))

            #Now we are going to store the shuffled rows in the matrix
            new_val_matrix = self.text_matrix[total_rows_done:total_rows_done + s,:]
            new_res_matrix = self.result_matrix[total_rows_done:total_rows_done + s,:]
                                        
            total_rows_done = total_rows_done + s

            #We build a list of the folds anad their result equivalents so we can
            #scroll through them later.
            self.val_comp_list.append(new_val_matrix)
            self.res_comp_list.append(new_res_matrix)

    def validation_testing_accuracy(self):
        #This function gives the accuracy of the method as well as computes the
        #confusion matrix and dif_vector
        accuracy_divider = self.test_comp_result_mat.shape[0]
        dif_vector = (self.test_comp_result_mat - self.testing_result_vector)
        num_bad_results = dif_vector.count_nonzero()
        #This is where the k-folds accuracy is computered
        accuracy = (accuracy_divider -  num_bad_results)/accuracy_divider

        #Here we list the sum of the all the records classified incorrectly.
        #This gives us all the words and their frequency that were in those
        #vectors.
        for result in range(0,dif_vector.shape[0]):
            if dif_vector[result,0]:
                self.word_confusion_vector = self.word_confusion_vector + self.testing_matrix[result,:]

        #The confusion matrix is calculated here
        if self.make_con_mat:
            for i in range(0,self.testing_result_vector.shape[0]):
                self.confusion_matrix[self.test_comp_result_mat[i,0]-1,self.testing_result_vector[i,0]-1] = \
                self.confusion_matrix[self.test_comp_result_mat[i,0]-1,self.testing_result_vector[i,0]-1] + 1
        #print("The accuracy of this validation set was " + str(self.accuracy[-1]) + ".")
        return accuracy
            
    def build_frequency_vector(self):
        #This function removes words that have either too low of a frequency
        # or two high of a frequncy in the data set.
        #print(self.text_matrix)

        #We start with building a single vector of total word count
        row_store = csr_matrix((1,self.text_matrix.shape[1]))
        #row_store = row_store.tocsr()
        #We pass through all rows and add them all together
        for num in range(0,self.result_matrix.shape[0]):
            row_store = self.text_matrix[num,:] + row_store
        np.set_printoptions(edgeitems=10)
        #We turn this all into one array
        temp_array = row_store.toarray()

        #print(temp_array.astype(int))

        col_clear = np.zeros((self.text_matrix.shape[0],1))
        col_clear = csr_matrix(col_clear)
        
        count = 0
        #This section just puts zero columns into the data where frequency
        #thresholds have been implemented.
        for col_j in range(0,self.text_matrix.shape[1]):
            if temp_array[0,col_j] > 0 \
               and temp_array[0,col_j] < self.lower_thresh:
                self.text_matrix[:,col_j] = col_clear
                #self.text_matrix[:,col_j] = self.text_matrix[:,col_j] * 0
                count = count + 1
                
            elif temp_array[0,col_j] > self.upper_thresh:
                self.text_matrix[:,col_j] = col_clear
                #self.text_matrix[:,col_j] = self.text_matrix[:,col_j] * 0
                count = count + 1

            elif temp_array[0,col_j] == 0:
                count + count + 1
        print("Total number of removed words: ", count)
        self.remove_words = count
        #print(self.text_matrix)
        #Everything below here is a visualization of the frequncy counts
        #These are used to help pick thresholds

        if self.view_freq == True:
            #These are the maximum and minimum number of times words appear in
            #the data set
            arg_max = int(temp_array.max())
            arg_min = int(temp_array.min())
            
            #The list content gives us a list of words to pair with the temp_array
            #to flesh out all words with the highest frequency of occurance.
            with open('vocabulary.txt','r') as file:
                content = file.readlines()
            content = [x.strip() for x in content]

            #This matrix keeps track of how many words occur in each frequncy bucket
            how_many = np.zeros((1,arg_max + 1))

            for kk in range(0,temp_array.shape[1]):
                how_many[0,int(temp_array[0,kk])] = how_many[0,int(temp_array[0,kk])] + 1

            #Here we print out the word with its freqency over a frequency threshold
            #we also print out the number of words that are over that threshold in total
            count = 0
            for kk in range(0,temp_array.shape[1]):
                if temp_array[0,kk] > self.upper_thresh:
                    print(content[kk], temp_array[0,kk])
                    count = count + 1
            print(count)

            print('\n')
            print(how_many.astype(int))
            print('\n')

            #Here we print out the list of values by 
            for ll in range(0,how_many.shape[1]):
                if how_many[0,ll]:
                    print(ll,how_many[0,ll])

    def normalize_text_matrix(self, matrix):
        #Normalize this by dividing all elements by their column sum
        c = matrix.sum(axis=0).A.ravel().astype(float)
        #c = matrix.max(axis=0).A.ravel().astype(float)
        a_to_stack = np.reciprocal(c, where = c != 0)
        #print(divisor.shape)
        a_to_stack = csr_matrix(a_to_stack)

        #this_matrix = matrix.multiply(divisor)
        this_matrix = lil_matrix(matrix.shape)
        for row in range(0,matrix.shape[0]):
            this_matrix[row,:] = lil_matrix(matrix.getrow(row).multiply(a_to_stack))

        this_matrix = csr_matrix(this_matrix)

        #This code below is an attempt to perform dimensionality reduction
        #this has been abandoned for now.
        
        #new_matrix = hstack([self.text_matrix,self.result_matrix])

        #col_dim = min(self.max_features,self.text_matrix.shape[0]-1,25)
        #u, s, vt = svds(new_matrix, col_dim, which = 'LM')
        #u = csr_matrix(u)
        #vt = csr_matrix(vt)
        #s = csr_matrix(diagsvd(s,u.shape[1],vt.shape[0]))
        #self.text_matrix = u*s*vt[:, col_dim]
        #self.result_matrix = u*s*vt[:, -1]

        return this_matrix

    def add_ones_col_to_matrix(self,matrix):
        #Note this vector is already normalized given that all the
        #columns are equal to 1.
        append_col = np.ones((matrix.shape[0],1))/matrix.shape[0]
        append_col = csr_matrix(append_col)

        matrix = csr_matrix(hstack([matrix, append_col]))

        return matrix

    def build_random_weight_matrix(self):
        #This function builds the initial random weight matrix.
        
        #init_weight_matrix = np.zeros((self.classes,self.text_matrix.shape[1]))
        init_weight_matrix = np.random.rand(self.classes,self.text_matrix.shape[1])
        init_weight_matrix = csr_matrix(init_weight_matrix)

        self.weight_matrix = init_weight_matrix

        #Also make the delta matrix here
        self.delta_matrix = csr_matrix(np.zeros((self.classes, \
                                                 self.text_matrix.shape[0])))
        #We make the negative delta matrix for a mask of the data later.
        self.neg_delta_matrix = csr_matrix(np.zeros((self.classes, \
                                                 self.text_matrix.shape[0])))

        #Increment through all the records we have and build the delta matrix
        for i in range(0,self.text_matrix.shape[0]):
            for j in range(0,self.classes):
                if int(self.result_matrix[i,0]) == int((j + 1)):
                    self.delta_matrix[j,i] = 1
                else:
                    self.neg_delta_matrix[j,i] = 1

    def find_prob_class_given_data(self):
        #Find exp(WX^T) here
        comb_vector = self.weight_matrix * self.text_matrix.transpose()
        exp_m_1 = comb_vector.expm1().todense()
        exp_m_1 = exp_m_1 + 1
        exp_m_1 = csr_matrix(exp_m_1)
        
        #Now replace all the elements where the truth is known with zeros
        exp_m_1 = exp_m_1.multiply(self.neg_delta_matrix)

        #Sum the whole matrix to get the normalization value
        #norm_constant = np.reciprocal(np.sum(exp_m_1.toarray()[:,:-1]) + 1)

        #Put ones in where the truth is known in the training
        exp_m_1 = exp_m_1 + self.delta_matrix

        #Normalize the matrix. This normalizes across the whole matrix
        #exp_m_1 = exp_m_1.multiply(norm_constant)

        #This normalizes by columns
        exp_m_1 = self.normalize_text_matrix(exp_m_1)

        if np.isnan(exp_m_1.data).any():
            self.prob_matrix = csr_matrix(np.nan_to_num(exp_m_1.toarray()))
        else:
            self.prob_matrix = exp_m_1

        #exp_m_1.data = np.where(exp_m_1.data < 0.00001, 0, exp_m_1.data)

    def find_new_weight_matrix(self):
        #This is the difference between our delta matrix and our prob matrix
        delta_prob_dif = self.delta_matrix - self.prob_matrix
        
        #This is for checking our maximum error if we want to do thresholding.
        max_dif = delta_prob_dif.multiply(delta_prob_dif).sum()

        #This is where we update the weights to the weight matrix. This is the
        #most important step.
        self.weight_matrix = self.weight_matrix + ((delta_prob_dif * self.text_matrix) \
                             - self.weight_matrix.multiply(self.regularization)).multiply(self.stepsize)

        #We have to keep track of overflow and underflow and set these values to
        #zero or the max or min allowed. This prevents NaNs from poping up in
        #weight then probability matrixes.
        if np.isnan(self.weight_matrix.toarray()).any():
            self.weight_matrix = csr_matrix(np.nan_to_num(self.weight_matrix.toarray()))

        return max_dif

    def classify(self, iteration = None):
        #We need to normalize the testing matrix with the same method we used
        #that is column by column normalization with the sum of the column.
        self.testing_matrix = self.normalize_text_matrix(self.testing_matrix)
        self.testing_matrix = self.add_ones_col_to_matrix(self.testing_matrix)

        #These steps build the classification matrix and normalize it.
        class_vector = self.weight_matrix * self.testing_matrix.transpose()
        exp_m_1 = class_vector.expm1().todense()
        exp_m_1 = exp_m_1 + 1
        exp_m_1 = csr_matrix(exp_m_1)        
        exp_m_1 = self.normalize_text_matrix(exp_m_1)

        #Here we remove any NaNs that have formed
        exp_m_1 = csr_matrix(np.nan_to_num(exp_m_1.toarray()))

        #This function thresholds the classification matrix to integers for
        #finding the 0s and 1s.
        classify_matrix = exp_m_1.rint().tocsc()

        #Make sure our matrix is only made up of integers for passing to the
        #.csv file.
        classify_matrix.astype(int)

        self.classify_matrix = classify_matrix
        
        #print(self.classify_matrix.shape)

        #This print out shows how full our matrix is and how many records we
        #have classification for in our testing set.
        #print(self.classify_matrix.count_nonzero())


        #Here we open a .csv file for printing out the results.
        if iteration:
            file = open('testing_results_LR_' + str(self.stepsize) + '_' + str(iteration) + '.csv' ,'w')
        else:
            file = open('testing_results_LR.csv' ,'w')
        file.write("id,class\n")

        result = None
        index = None

        #We break down the classification matrix into its components
        self.testing_result_vector = []
        row, col, value = find(classify_matrix)

        #We see what columns have a value in them. The columns represent the
        #records in the testing matrix.
        for col_v in range(0, classify_matrix.shape[1]):
            #If a given column number is in the numpy array
            if col_v in col[:]:
                #Then we find its index.
                index = np.where(col == col_v)
                #print("Type 01 executed.")
                #This was incase we had more than one value
                #in a column
                if len(index) > 1:
                    index = np.random.choice(len(index),1)
                    #print("Type 002 executed.")
                #Set the result to be the document number via row + 1
                result = int(row[index[0]]) + 1
            else:
                #If no value exists for that column. Then guess.
                result = np.random.choice(20,1)[0] + 1
                #print("Type 3 executed.")
                #result = 0
            #Append the results to a result vector for validation
            self.testing_result_vector.append(result)
            #Print out the result for kaggle in a .csv
            file.write(str(self.text_matrix.shape[0] + col_v + 1) + "," + str(result) + "\n")
        file.close()
        #Save the result vector globally for validation usage.
        self.testing_result_vector = csr_matrix(self.testing_result_vector).transpose()

        #Remove the extra column we added to the testing matrix
        self.testing_matrix = self.testing_matrix[:,:-1]

if __name__ == '__main__':
    average_list = []
    for item0 in [i/10000 for i in range(100, 110, 10)]:
        #start_time = datetime.now()
        test_run = Logistic_Regression_Classifier('training_matrix_rows_1000.npz', \
                                              'result_matrix_rows_1000.npz', \
                                              'testing_matrix.npz', \
                                              val_run = True, \
                                              k_folds = 3, \
                                              iteration_number = 20000, \
                                              stepsize = item0, \
                                              regularization = 0.009)
        #print("Validation time taken was, ", datetime.now() - start_time)        
        average_list.append((item0,test_run.regularization,test_run.average_accuracy))
        print("So far our list of results is: ", average_list)
    print(average_list)
