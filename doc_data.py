#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Authors: Christopher Zapotocky and Francisco Viramontes
Created: 09/23/2018
Modified: 09/23/2018
Program Name: doc_data
Program Purpose: The programs purpose is to load the data in for both programs.
We will make a standard form for both to crunch the data and then save .npz
files to quickly load the data for building of the models.
"""
import numpy as np
from scipy.sparse import save_npz
from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
#import pandas as pd #pd.read_csv('_.csv')
from datetime import datetime
import time

class data_model:
    def __init__(self,file_name = 'training.csv'):
        self.file_name = file_name
        ########################################################################
        #Get vocab file length
        vocab_file_lenth = self.count_rows_in_file('vocabulary.txt')      
        ########################################################################        
        #Get number of rows
        row_count = self.count_rows_in_file(file_name)
        #row_count = 1000
        ########################################################################
        #Get width of matrix
        col_count = self.count_cols_in_file(file_name)
        ########################################################################

        train_file = open(file_name,'r')

        if file_name == 'training.csv':
            col_count = col_count - 1
            result_matrix = lil_matrix((row_count,1), dtype=int)

        text_matrix = lil_matrix((row_count, col_count), dtype=int)

        start_time = datetime.now()

        #Go through all the rows in the training data and build a matrix
        for i in range(0,row_count):
            current_row_string = train_file.readline()
            current_row_string = current_row_string.strip()

            first_comma_i = current_row_string.find(',')

            store_string = ''
            temp_num_list = []

            if file_name == 'training.csv':
                #The [::-1] reverses the string
                last_comma_i = current_row_string[::-1].find(',')
                #Strip out the first and last columns.
                result_matrix[i,0] = int(current_row_string[-last_comma_i:])
                current_row_string = current_row_string[first_comma_i + 1:-last_comma_i]
            elif file_name == 'testing.csv':
                current_row_string = current_row_string[first_comma_i + 1:] + ','

            comma_number = 0
            #Fill the rows of the text matrix column by column
            for j in range(0,len(current_row_string)):
                if current_row_string[j] == ',':
                    store_string = ''.join(temp_num_list)
                    text_matrix[i,comma_number] = int(store_string)
                    temp_num_list = []
                    comma_number = comma_number + 1
                else:
                    temp_num_list.append(current_row_string[j])

        text_matrix = text_matrix.tocsr()

        if file_name == 'training.csv':    
            result_matrix = result_matrix.tocsr()
            save_npz(file_name[:-4] + "_matrix_rows_" + str(row_count) + ".npz",text_matrix) 
            save_npz("result_matrix_rows_" + str(row_count) + ".npz",result_matrix)
        else:
            save_npz(file_name[:-4] + "_matrix.npz",text_matrix)            
        
        print("The total time to run and store everything was, ", datetime.now() - start_time)
        
    def count_rows_in_file(self,file_name):
        file = open(file_name,'r')

        file_length = 0

        while file.readline():
            file_length = file_length + 1
                
        file.close()

        return file_length

    def count_cols_in_file(self,file_name):
        file = open(file_name,'r')

        col_count = 0    

        for char in file.readline():
            if char == ',':
                col_count = col_count + 1

        file.close()

        return col_count
