#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Authors: Christopher Zapotocky and Francisco Viramontes
Created: September 23, 2018
Modified: October 13, 2018
Program Name: naiveBayes.py
Program Function: The program performs the Naive Bayes
classification on a set of data. Particularly a set of
word frequencies taken from a number of documents.
"""

#Incase the matricies we request are not there
import doc_data
import numpy as np
#Import all the sparse matricies libraries
from scipy.sparse import vstack, hstack, diags, eye, find
from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
from scipy.sparse import load_npz
#We need os.path to go find the saved sparse matrices
import os.path
#We need dattime to run the timer to see how we are doing
from datetime import datetime
#We need math because math is important...and we need to take logs
import math
#This is experimentally used. I was trying to remove words from the matrix
#by zeroing out there probability. Works like log normally, but sets
#log(0) = 0
from scipy.special import xlogy
#Remember lists are not deep copied and can be effected by replacement 
import copy
#We need this to shuffle the row values to make the validation sets
import random
import time

class Naive_Bayes_Classifier:
    def __init__(self,training_name = 'training_matrix.npz', \
                 result_name = 'result_matrix.npz', \
                 testing_name = 'testing_matrix.npz', \
                 vocab_list_name = 'vocabulary.txt', \
                 model_tune = True, \
                 lower_thresh = 0, \
                 upper_thresh = 200000, \
                 view_freq = False, \
                 beta_set = False, \
                 beta = 1, \
                 val_run = False, \
                 k_folds = 8, \
                 make_con_mat = False, \
                 make_word_con_vec = False, \
                 mutual_information = False):
        if not (os.path.exists(training_name)) \
            and not(os.path.exists(result_name)):
            model = doc_data.data_model()

        self.vocab_list_name = vocab_list_name
        self.vocab_list = self.get_vocab_list()

        #Load matrices for computation of Naive Bayes
        self.text_matrix = load_npz(training_name)
        self.result_matrix = load_npz(result_name)
        self.testing_matrix = load_npz(testing_name)

        #Here we get the number of classes in the result matrix this is
        #important to the program.
        self.classes = int(self.result_matrix.max())

        #Stored class variables during initialization
        self.upper_thresh = upper_thresh
        self.lower_thresh = lower_thresh
        self.beta_set = beta_set
        self.beta = beta
        self.k_folds = k_folds
        self.view_freq = view_freq
        self.removed_words = 0
        self.accuracy = []
        self.average_accuracy = None
        self.make_con_mat = make_con_mat
        self.make_word_con_vec = make_word_con_vec
        self.mutual_information = mutual_information
        #We ask here if this is a validation run if so then validation subsets
        #are built and the self.text_matrices and self.result_matrices are
        #varied. Else...we run through the full data set.
        self.val_run = val_run

        #This is just to check how long the program takes to run.
        start_time = datetime.now()
        print("The function started at: ", start_time)

        #Only run the model tuning if we postively select it
        if model_tune == True:
            self.build_frequency_vector()

        #For a kaggle run we want to use the full data set with no validation
        #the if here does that        
        if not val_run:
            self.primary_function_calls()
        #If validation is indeed set then we run the else statement here
        else:
            #This function calculate the k-folds that are necessary to build
            #the validation matrices on every step both text and result
            self.build_k_fold_validation()

            #Here we instantiate for the confusion matrix and
            #word_confusion_vector
            self.confusion_matrix = np.zeros((self.classes,self.classes))
            self.word_confusion_vector = np.zeros((1,self.testing_matrix.shape[1]))
            self.word_confusion_vector = csr_matrix(self.word_confusion_vector)

            #After building our validation partition we have to rebuild the text
            #matrix minus one partition.
            for part in range(0,self.k_folds):
                print("The current partition being tested is partition " + str(part) + ".")
                #For this round do the following:
                
                #Build the validation training set
                temp_train_list = copy.deepcopy(self.val_comp_list)
                del temp_train_list[part]

                #Build the validtion training result set
                temp_result_list = copy.deepcopy(self.res_comp_list)
                del temp_result_list[part]

                #Build the validation testing result set
                temp_test_result_list = copy.deepcopy(self.res_comp_list[part])

                #Build the validation testing set
                temp_test_mat = copy.deepcopy(self.val_comp_list[part])

                #We constantly rebuild the full training set by stacking the set
                #of sparse matrices
                self.text_matrix = vstack(temp_train_list)
                self.result_matrix = vstack(temp_result_list)

                #Since we only have one partition being tested...not stacking is
                #needed in this part
                self.testing_matrix = temp_test_mat
                self.test_comp_result_mat = temp_test_result_list

                #Run everything with the primary functions as previously.
                self.primary_function_calls()
                self.validation_testing_accuracy()

            self.average_accuracy = sum(self.accuracy)/len(self.accuracy)             
            print("The average accuracy at " + str(self.beta) + " is " \
                    + str(self.average_accuracy) + ".")
            print("\n")
            #This prints the confusion matrix and word_confusion_vector if
            #we have those flags set
            if self.make_word_con_vec:
                np.set_printoptions(threshold=numpy.nan)
                print(self.word_confusion_vector)
            if self.make_con_mat:            
                np.set_printoptions(linewidth = 180)
                print(self.confusion_matrix)
        #We print out the time taken for the validation run here.
        print("Function time taken: ", datetime.now() - start_time)
                
    def primary_function_calls(self):
        #This function simply compartmentalizes the main call of the function
        #pieces
        self.build_prior_matrix()
        self.word_freq_matrix = csr_matrix((self.classes,self.text_matrix.shape[1]))
        self.build_likelihood_matrix()
        self.classify_opt()
        if self.mutual_information:
            self.make_mutual_information_matrix()

    def get_vocab_list(self):
        with open(self.vocab_list_name,'r') as file:
            content = file.readlines()
            content = [x.strip() for x in content]        
        file.close()

        return content

    def build_k_fold_validation(self):
        #We assume the user wants a "leave one partition out" k_folds
        #validation here
        
        #This is going to be a list of pointers to the matricies
        #specified by the kfolds cross validiation
        self.val_comp_list = []
        self.res_comp_list = []

        #Make a list of the row numbers for the text matrix.
        row_num_list = [j for j in range(0,self.text_matrix.shape[0])]

        #Then we shuffle the row numbers to make a randomization of the
        #row numbers. This is done inplace...so no need to assign the output.
        random.shuffle(row_num_list)

        #Now that we have randomly shuffled the row numbers we are going to
        #take all the n mod(k) = 0 and put it in partition 0. We will do the
        #same for 1,2, up to k-1. That will give us our k, random, partitions.

        k_fold_rem = self.text_matrix.shape[0] % self.k_folds
        j = math.floor(self.text_matrix.shape[0]/self.k_folds)
        total_rows_done = 0

        for row_mod in range(0,self.k_folds):
            #We want to balance our valdiation as much as possible so we see
            #what the remainder is from the division by k-folds. Our first j
            #matrices we build will have 1 extra row over the
            #floor(rows_text_matrix/k_folds).

            #Which side of the excess from the remainder are we on right now?
            if row_mod < k_fold_rem:
                s = j + 1
            else:
                s = j

            #Build the validation matrix partition
            new_val_matrix = csr_matrix((s,self.text_matrix.shape[1]))
            new_res_matrix = csr_matrix((s,1))

            #Now we are going to store the shuffled rows in the matrix
            new_val_matrix = self.text_matrix[total_rows_done:total_rows_done + s,:]
            new_res_matrix = self.result_matrix[total_rows_done:total_rows_done + s,:]
                                        
            total_rows_done = total_rows_done + s

            #We build a list of the folds anad their result equivalents so we can
            #scroll through them later.
            self.val_comp_list.append(new_val_matrix)
            self.res_comp_list.append(new_res_matrix)

    def validation_testing_accuracy(self):
        #This function gives the accuracy of the method as well as computes the
        #confusion matrix and dif_vector
        accuracy_divider = self.test_comp_result_mat.shape[0]
        dif_vector = (self.test_comp_result_mat - self.testing_result_vector)
        num_bad_results = dif_vector.count_nonzero()
        #This is where the k-folds accuracy is computered
        self.accuracy.append((accuracy_divider -  num_bad_results)/accuracy_divider)

        #Here we list the sum of the all the records classified incorrectly.
        #This gives us all the words and their frequency that were in those
        #vectors.
        for result in range(0,dif_vector.shape[0]):
            if dif_vector[result,0]:
                self.word_confusion_vector = self.word_confusion_vector + self.testing_matrix[result,:]

        #The confusion matrix is calculated here
        if self.make_con_mat:
            for i in range(0,self.testing_result_vector.shape[0]):
                self.confusion_matrix[self.test_comp_result_mat[i,0]-1,self.testing_result_vector[i,0]-1] = \
                self.confusion_matrix[self.test_comp_result_mat[i,0]-1,self.testing_result_vector[i,0]-1] + 1
        #print("The accuracy of this validation set was " + str(self.accuracy[-1]) + ".")
        
    def build_prior_matrix(self):
        #We need the maximum class value of the results to make the prior
        #vector
        arg_max = int(self.result_matrix.max())
        prior_m = np.zeros((arg_max,1))

        #Here we get the counts for the prior matrix and fill it
        for el in range(0,self.result_matrix.shape[0]):
            prior_m[self.result_matrix[el,0]-1,0] = prior_m[self.result_matrix[el,0]-1,0] + 1

        #We normalize to make it a probability vector
        prior_tot = prior_m.sum()
        prior_m = prior_m/prior_tot
        self.prior_sparse = csr_matrix(prior_m)

        #We also make a vector with the log of all values for future speed up
        log_prior_m = np.log2(prior_m)
        log_prior_tot = np.log2(prior_tot)
        log_prior_m = log_prior_m/log_prior_tot
        self.log_prior_sparse = csr_matrix(log_prior_m)

    def build_likelihood_matrix(self):
        #This function build the likelihood matrix
        #Starting with the text matrix we make a probability and log probability
        #matrix.
        self.text_prob_matrix = csr_matrix((self.classes,self.text_matrix.shape[1]))
        self.log_text_prob_matrix = csr_matrix((self.classes,self.text_matrix.shape[1]))
        for ii in range(0,self.classes):
            row_store = csr_matrix((1,self.text_matrix.shape[1]))
            for num in range(0,self.result_matrix.shape[0]):
                if np.equal(ii,int(self.result_matrix[num,0])-1):
                    row_store = self.text_matrix[num,:] + row_store
            self.word_freq_matrix[ii,:] = row_store
            class_sum = row_store.sum()
            vocab_tot = row_store.shape[1]
            #We only change the beta value if the beta_set flag is true
            if self.beta_set == False:
                self.beta = float(1/(vocab_tot-self.removed_words))

            #This is our numerator fudge factor for avoiding zeros in the
            #probability matrix
            one_scalar = np.ones((1,row_store.shape[1])) * (self.beta)
            one_scalar = csr_matrix(one_scalar)

            #Here is where we fill the probability matrix row by row
            self.text_prob_matrix[ii,:] = (row_store + one_scalar) \
            / (class_sum + (self.beta)*(vocab_tot-self.removed_words))

            #This piece performs a special log function due to the posibility of
            #zeros in the matrix. I cut that part out, but the function still
            #works fine. This is where we build the log probability matrix.
            temp_full_array = self.text_prob_matrix[ii,:].toarray()
            temp_log_array = xlogy(np.sign(temp_full_array), temp_full_array) / np.log(2)
            self.log_text_prob_matrix[ii,:] = csr_matrix(temp_log_array)

    def build_frequency_vector(self):
        #This function removes words that have either too low of a frequency
        # or two high of a frequncy in the data set.
        #print(self.text_matrix)

        #We start with building a single vector of total word count
        row_store = csr_matrix((1,self.text_matrix.shape[1]))
        #row_store = row_store.tocsr()
        #We pass through all rows and add them all together
        for num in range(0,self.result_matrix.shape[0]):
            row_store = self.text_matrix[num,:] + row_store
        np.set_printoptions(edgeitems=10)
        #We turn this all into one array
        temp_array = row_store.toarray()

        #print(temp_array.astype(int))

        col_clear = np.zeros((self.text_matrix.shape[0],1))
        col_clear = csr_matrix(col_clear)
        
        count = 0
        #This section just puts zero columns into the data where frequency
        #thresholds have been implemented.
        for col_j in range(0,self.text_matrix.shape[1]):
            if temp_array[0,col_j] > 0 \
               and temp_array[0,col_j] < self.lower_thresh:
                self.text_matrix[:,col_j] = col_clear
                #self.text_matrix[:,col_j] = self.text_matrix[:,col_j] * 0
                count = count + 1
                
            elif temp_array[0,col_j] > self.upper_thresh:
                self.text_matrix[:,col_j] = col_clear
                #self.text_matrix[:,col_j] = self.text_matrix[:,col_j] * 0
                count = count + 1

            elif temp_array[0,col_j] == 0:
                count + count + 1
        print("Total number of removed words: ", count)
        self.remove_words = count
        #print(self.text_matrix)
        #Everything below here is a visualization of the frequncy counts
        #These are used to help pick thresholds

        if self.view_freq == True:
            #These are the maximum and minimum number of times words appear in
            #the data set
            arg_max = int(temp_array.max())
            arg_min = int(temp_array.min())
            
            #The list content gives us a list of words to pair with the temp_array
            #to flesh out all words with the highest frequency of occurance.
            with open('vocabulary.txt','r') as file:
                content = file.readlines()
            content = [x.strip() for x in content]

            #This matrix keeps track of how many words occur in each frequncy bucket
            how_many = np.zeros((1,arg_max + 1))

            for kk in range(0,temp_array.shape[1]):
                how_many[0,int(temp_array[0,kk])] = how_many[0,int(temp_array[0,kk])] + 1

            #Here we print out the word with its freqency over a frequency threshold
            #we also print out the number of words that are over that threshold in total
            count = 0
            for kk in range(0,temp_array.shape[1]):
                if temp_array[0,kk] > self.upper_thresh:
                    print(content[kk], temp_array[0,kk])
                    count = count + 1
            print(count)

            print('\n')
            print(how_many.astype(int))
            print('\n')

            #Here we print out the list of values by 
            for ll in range(0,how_many.shape[1]):
                if how_many[0,ll]:
                    print(ll,how_many[0,ll])

    #This function is very slow...about 100 times slower than classify_opt
    #in the project problem.
    def classify(self):
        #print("The classify function was started at: ", datetime.now())
        file = open('testing_results.csv','w')
        file.write("id,class\n")
        for row_i in range(0, self.testing_matrix.shape[0]):
            #start_time = datetime.now()
            class_value_store = []
            #Loop through the columns and the rows and perform element by
            #element classification
            for ii in range(0,self.classes):
                current_class_value = 0
                for col_i in range(0, self.testing_matrix.shape[1]):
                    #Here we perform the classification step
                    if self.testing_matrix[row_i,col_i] > 0 \
                        and self.word_freq_matrix[ii,col_i] > 0:
                            current_class_value = current_class_value + \
                            self.testing_matrix[row_i,col_i] * math.log(self.text_prob_matrix[ii,col_i],2)
                #We add the log prior to get the final result
                current_class_value = current_class_value + math.log(self.prior_sparse[ii,0],2)
                class_value_store.append(current_class_value)
            #This returns the final result with the off by one adjustment
            classify_result = class_value_store.index(max(class_value_store)) + 1
            file.write(str(self.text_matrix.shape[0] + row_i + 1) + "," + str(classify_result) + "\n")
            #print('Finished row: ' + str(self.text_matrix.shape[0] + row_i + 1))
            #print("Row time taken, ", datetime.now() - start_time)
        file.close()

        print("The classify function ended at: ", datetime.now())
    #This is the optimized classification function. It I recommend this one be
    #used for any sort computational run. 
    def classify_opt(self):
        self.testing_result_vector = []
        #print("The classify function was started at: ", datetime.now())
        
        #Here we open a .csv file for printing out the results.
        file = open('testing_results.csv','w')
        file.write("id,class\n")
        #This for loop provides the iteration through the rows to perform
        #classification on the testing set.
        for row_i in range(0, self.testing_matrix.shape[0]):
            start_time = datetime.now()
            #Here is where we actually perform the classification
            class_value_store = (self.log_text_prob_matrix * \
                                       self.testing_matrix[row_i,:].transpose())\
                                       + self.log_prior_sparse
            #We still need a plus one to adjust for the row difference
            classify_result = class_value_store.argmax() + 1
            #Append the results to a result vector for validation
            self.testing_result_vector.append(classify_result)
            #Print out the result for kaggle in a .csv
            file.write(str(self.text_matrix.shape[0] + row_i + 1) + "," + str(classify_result) + "\n")
            #print('Finished row: ' + str(self.text_matrix.shape[0] + row_i + 1))
            #print("Row time taken, ", datetime.now() - start_time)
        file.close()
        #Save the result vector globally for validation usage.
        self.testing_result_vector = csr_matrix(self.testing_result_vector).transpose()

    def make_mutual_information_matrix(self):
        #First we need to make P(x)
        x_prob = csr_matrix(self.word_freq_matrix.sum(axis=0) \
                 /self.word_freq_matrix.sum())
        x_prob = x_prob.toarray()

        #it is cleaner to deal with the log of x_prob
        log_x_prob = csr_matrix(xlogy(np.sign(x_prob), x_prob))

        #Make our storage matrix
        mutual_info_matrix = csr_matrix(self.log_text_prob_matrix.shape)

        #Here we use the property of the logarithm to fill MI Matrix
        for i in range(0,self.log_text_prob_matrix.shape[0]):
            mutual_info_matrix[i,:] = self.log_text_prob_matrix[i,:] - log_x_prob

        #We change to lil_matricies because they are faster for assignment
        mutual_info_matrix = lil_matrix(mutual_info_matrix)
        self.text_prob_matrix = lil_matrix(self.text_prob_matrix)
        self.prior_sparse = lil_matrix(self.prior_sparse)

        temp_prior_matrix = lil_matrix(self.text_prob_matrix.shape)

        #Store the sparse matrix
        for j in range(0,self.text_prob_matrix.shape[1]):
            temp_prior_matrix[:,j] = self.prior_sparse

        #Solve for the joint probability of x,y for a specific x. Then multiply
        #by the log term from above
        mutual_info_matrix = self.text_prob_matrix.multiply( \
                             mutual_info_matrix.multiply( \
                             self.prior_sparse))

        #Sum over all the classes to get the MI for each word given every class
        mutual_info_matrix = mutual_info_matrix.sum(axis=0)

        #Convert everything back to CSR_matrices
        mutual_info_matrix = csr_matrix(mutual_info_matrix)
        self.text_prob_matrix = csr_matrix(self.text_prob_matrix)
        self.prior_sparse = csr_matrix(self.prior_sparse)
        

        #Pull out the information
        row, col, value = find(mutual_info_matrix)

        ind = list(np.argpartition(value, -100)[-100:])

        reorder_by_MI = len(value)
        
        full_ind = list(np.argpartition(value,-reorder_by_MI)[-reorder_by_MI:])

        max_word_list = [self.vocab_list[index] for index in ind]

        print(max_word_list)

        #sorted_by_MI = [self.vocab_list[index] for index in full_ind]
        #print(sorted_by_MI)


if __name__ == '__main__':
    #average_list = []
    #for item0 in [i/10000 for i in range(1, 10100, 100)]:
    #    start_time = datetime.now()
    #    test_run = Naive_Bayes_Classifier('training_matrix_rows_12000.npz', \
    #                                      'result_matrix_rows_12000.npz', \
    #                                      'testing_matrix.npz', \
    #                                      val_run = True, \
    #                                      beta_set = True, \
    #                                      beta = item0, \
    #                                      k_folds = 12)
    #    print("Validation time taken was, ", datetime.now() - start_time)        
    #    average_list.append((item0,test_run.average_accuracy))
    #print(average_list)
    start_time = datetime.now()
    test_run = Naive_Bayes_Classifier('training_matrix_rows_12000.npz', \
                                          'result_matrix_rows_12000.npz', \
                                          'testing_matrix.npz', \
                                          val_run = False, \
                                          beta_set = True, \
                                          beta = 1/61188, \
                                          k_folds = 12, \
                                          upper_thresh = 200000, \
                                          make_con_mat = False, \
                                          view_freq = False)
    print("Total time was, ", datetime.now() - start_time)  
