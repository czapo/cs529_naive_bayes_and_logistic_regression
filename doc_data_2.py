#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Authors: Christopher Zapotocky and Francisco Viramontes
Created: 09/23/2018
Modified: 09/23/2018
Program Name: doc_data
Program Purpose: The programs purpose is to load the data in for both programs.
We will make a standard form for both to crunch the data and then save .npz
files to quickly load the data for building of the models.
"""
import numpy as np
from scipy.sparse import save_npz
from scipy.sparse import lil_matrix
from scipy.sparse import csr_matrix
#import pandas as pd #pd.read_csv('_.csv')
from datetime import datetime
import time
from numpy import genfromtxt


class data_model:
    def __init__(self,file_name = 'training.csv'):
        my_data = genfromtxt(file_name, delimiter=',')

        print(my_data[1,:])
